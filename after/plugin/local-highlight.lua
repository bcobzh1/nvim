require('local-highlight').setup({
    file_types = { 'python', 'lua', 'rust', 'helm', 'yaml' },
    hlgroup = 'TSDefinitionUsage',
    cw_hlgroup = nil,
})
