local harpoon = require("harpoon")

-- REQUIRED
harpoon:setup()
-- REQUIRED

vim.keymap.set("n", "<leader>a", function() harpoon:list():append() end)
vim.keymap.set("n", "<M-e>", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)

vim.keymap.set("n", "<M-j>", function() harpoon:list():next() end)
vim.keymap.set("n", "<M-k>", function() harpoon:list():prev() end)
vim.keymap.set("n", "<M-u>", function() harpoon:list():select(1) end)
vim.keymap.set("n", "<M-i>", function() harpoon:list():select(2) end)
vim.keymap.set("n", "<M-o>", function() harpoon:list():select(3) end)
vim.keymap.set("n", "<M-p>", function() harpoon:list():select(4) end)
