-- lua/custom/plugins/mini.lua
return {
    {
        'echasnovski/mini.nvim',
        enabled = true,
        config = function()
            local statusline = require 'mini.statusline'
            statusline.setup { use_icons = true }
            require('mini.ai').setup()
            require('mini.completion').setup()
            require('mini.icons').setup()

            local gen_loader = require('mini.snippets').gen_loader
            require('mini.snippets').setup({
                snippets = {

                    -- Load snippets based on current language by reading files from
                    -- "snippets/" subdirectories from 'runtimepath' directories.
                    gen_loader.from_lang(),
                },
            })
        end
    },
    { "rafamadriz/friendly-snippets" }
}
